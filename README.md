### Steps to start the application

1 -  Install all dependencies by going to project root

    command: yarn

2 - In one terminal run below command

    command: yarn webpack
    
3 - Then Start the node server using below command

    command: npm dev
    
4 - Check the application on this url - http://localhost:8088


#### Eexcute tests using below url

    command: npm test