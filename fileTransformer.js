// fileTransformer.js
const path = require('path');

module.exports = {
  process(src, filename, config, options) {
    console.info(options);
    return 'module.exports = ' + JSON.stringify(path.basename(filename)) + ';';
  },
};