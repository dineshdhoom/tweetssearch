class DataApi {

  constructor(rawData) {
    this.rawData = rawData;
  }

  mapIntoObject(arr) {
    return arr.reduce((acc, curr) => {
      acc[curr.id_str] = this.filterObject(curr);
      return acc;
    }, {});
  }

  getTweets() {
    return this.mapIntoObject(this.rawData);
  }

  filterObject(tweet) {
    return {
      id: tweet.id_str,
      created_at: tweet.created_at,
      text: tweet.text,
      user_name: tweet.user.name,
      user_screen_name: tweet.user.screen_name,
      user_profile_image_url: tweet.user.profile_image_url,
      media_url: tweet.entities.media && tweet.entities.media.length ? tweet.entities.media[0].media_url : null,
      media_url_https: tweet.entities.media && tweet.entities.media.length ? tweet.entities.media[0].media_url_https : null,
    };
  }
}

export default DataApi;