import DataApi from '../DataApi';
import { statuses } from '../testData';

const api = new DataApi(statuses);

describe('DataApi', () => {

  it('expects statuses as an tweet object', () => {
    const tweets = api.getTweets();
    const tweetId = statuses[0].id_str;
    const tweetUserName = statuses[0].user.name;
    const tweetText = statuses[0].text;
    const tweetUserScreenName = statuses[0].user.screen_name;
    expect(tweets).toHaveProperty(tweetId);
    expect(tweets[tweetId].user_name).toBe(tweetUserName);
    expect(tweets[tweetId].user_screen_name).toBe(tweetUserScreenName);
    expect(tweets[tweetId].text).toBe(tweetText);
  });

});
