import axios from 'axios';
const Config  = require('config');

export default {
  tweets: (keywrods) => {
    console.info('keywords', keywrods);
    return axios(`/search/tweets?q=${keywrods}`, {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${Config.apiToken}`,
      },
    }).then((res) => {
      return res;
    });
  }
};
