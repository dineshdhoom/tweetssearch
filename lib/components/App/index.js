import React from 'react';

import { TweetList } from '../TweetList';
import { SearchBar } from '../SearchBar';
import ReactLoading from 'react-loading';
import DataApi from '../../DataApi';
import styles from './style.css';
import { loadTweets } from '../../redux/actions/tweets';
import { connect } from 'react-redux';

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      tweets: Object.keys(this.props.tweets).length > 0  ? this.props.tweets : {},
      isLoading: false,
      hasError: false, 
      error: null,
    };
  }

  render() {
    const { hasError, isLoading } = this.state;
    return(
      <div>
        <SearchBar onSearch={this.handleSearch} />
        {Object.keys(this.state.tweets).length > 0 && <TweetList tweets={this.state.tweets} /> }
        {hasError && <span className={styles.error}><h3>Something went wrong...Please try entering other keywords</h3></span>}
        {isLoading && <div className={styles.loading}><ReactLoading type='bars' color='#444' /></div>}
      </div>
    );
  }

  handleSearch = (keywrods) => {
    this.setState({isLoading: true});
    this.props.loadTweets(keywrods).then((response) => {
      const statuses = response.statuses;
      const tweets = transformTweets(statuses);   

      this.setState({isLoading: false, tweets});
    }).catch((error) => {
      this.setState({isLoading: false, hasError: true, error});
    });
  }
}

const transformTweets = (rawData) => {
    const api = new DataApi(rawData);
    return api.getTweets();
}

const mapStateToProps = (state) => {
  return {
    tweets: transformTweets(state.tweetData.tweets)
  };
};

export default connect(mapStateToProps, { loadTweets })(App);