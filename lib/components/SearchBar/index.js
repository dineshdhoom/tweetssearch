import React from 'react';

import styles from './style.css';
import * as FontAwesome from 'react-icons/lib/fa';

class SearchBar extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      keywords: '',
    };
  }

  render() {
    return (
      <div className={styles.searchBarWrap}>
        <div className={styles.search}>
          <input type="text" value={this.state.keywords} className={styles.searchTerm} placeholder="Search for tweets" onChange={this.handleChange} />
          <button type="submit" className={styles.searchButton} onClick={this.handleSearch}>
            <FontAwesome.FaSearch />
          </button>
        </div>
      </div>
    );
  }

  handleChange = (e) => {
    this.setState({keywords: e.target.value});
  }

  handleSearch = () => {
    this.props.onSearch(this.state.keywords);
  }
}

export { SearchBar };