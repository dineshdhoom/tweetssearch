import React from 'react';
import styles from './style.css';
import { Link as ReactRouterLink } from 'react-router-dom';
const Radium = require('radium');

const Link = Radium(ReactRouterLink);
const LinkStyle = {
  hoverEffect: {
    ':hover': {
      borderStyle: 'dashed',
      backgroundColor: '#dedede',
    }
  }
};

const Tweet = (props) => {
  const { user_profile_image_url, user_name, text, id, created_at }  = props.tweet;

  return (
    <Link to={`/tweet/${id}`}>
      <div className={styles.gridItem} style={LinkStyle.hoverEffect}>
        <div className={styles.meta}>
          <h3 className={styles.itemTitle}>
            <div className={styles.userData}>
              <div className={styles.user}>
                <img className={styles.userImage} src={user_profile_image_url} alt="" width="20" height="20" />
                <span className={styles.usernameText}>{user_name}</span>
              </div>
            </div>
          </h3>
          <p className={styles.tweetText}> {text} </p>
          <p> {dateDisplay(created_at)} </p>
        </div>
      </div>
    </Link>
  );
};

const dateDisplay = (dateString) => {
  return `${formatAMPM(new Date(dateString))} - ${new Date(dateString).toDateString()}`;
};

const formatAMPM = (date) => {
  var hours = date.getHours();
  var minutes = date.getMinutes();
  var ampm = hours >= 12 ? 'PM' : 'AM';
  hours = hours % 12;
  hours = hours ? hours : 12;
  minutes = minutes < 10 ? '0'+minutes : minutes;
  var strTime = hours + ':' + minutes + ' ' + ampm;
  return strTime;
};

export { Tweet };