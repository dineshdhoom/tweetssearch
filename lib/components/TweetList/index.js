import React from 'react';

import { Tweet } from '../Tweet';
import styles from './style.css';

const TweetList = (props) => {
  return (
    <div className={styles.gridWraper}>
      <div className={styles.gridList}>
        {Object.values(props.tweets).map((tweet) => <Tweet key={tweet.id} tweet={tweet} /> )}
      </div>
    </div>
  );
};

export { TweetList };