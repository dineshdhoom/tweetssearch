import React from 'react';

import 'babel-polyfill';
import { TweetList }  from './index';

import renderer from 'react-test-renderer';
import { StaticRouter } from 'react-router-dom';

describe('TweetList', () => {

  const testProps = {
    tweets: {
      a: {id: 'a'},
      b: {id: 'b'},
    }
  };

  it('renders correctly', () => {
    let context = {};
    const tree = renderer.create(
      <StaticRouter location="someLocation" context={context}>
        <TweetList {...testProps} />
      </StaticRouter>
    ).toJSON();
    expect(tree.children.length).toBe(1);
    expect(tree).toMatchSnapshot();
  });

});