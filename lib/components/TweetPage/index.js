import React from 'react';

import styles from './style.css';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom'

const mapStateToProps = (state) => {
  return {
    tweets: state.tweetData.tweets
  };
};

class TweetPage extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { tweets } = this.props;
    const tweetId = this.props.match.params.id;
    const tweet = tweets.filter(function(item) {return item.id_str === tweetId; });
    
    return (
      <div className="span-12">
        <div className={styles.descriptionWrapper}>
          <Link className={styles.backLink} to='/'>Back</Link>
          <ol className={styles.nameValue}>
            <li>                    
              <label htmlFor="about">Name</label>
              <span id="about">{tweet[0].user.name}</span>
            </li>
            <li>
              <label htmlFor="about">Created Date</label>
              <span id="Span1">{tweet[0].created_at}</span>
            </li>
            <li>
              <label htmlFor="about">Description</label>
              <span id="Span2">{tweet[0].text}</span>
            </li>
            <li>
              <label htmlFor="url">Tweet URL</label>
              <span id="Span2"><a target="_blank" href={`https://twitter.com/statuses/${tweetId}`} >{`https://twitter.com/statuses/${tweetId}`}</a></span>
            </li>
          </ol>
      </div>
      </div>
    ); 
  }
}

export default connect(mapStateToProps, null)(TweetPage);