module.exports = {
  port: process.env.PORT || 8088,
  apiUrl: 'https://api.twitter.com/1.1/search/tweets.json',
  apiToken: '<YOUR_API_TOKEN>',
};