import {
  LOAD_TWEETS_REQUEST,
  LOAD_TWEETS_FAILURE,
  LOAD_TWEETS_SUCCESS,
} from '../../type';

import api from '../../../api/tweets';

export const loadTweetsRequest = (data) => ({
  type: LOAD_TWEETS_REQUEST,
  data,
});

export const loadTweetsSuccess = (tweets) => ({
  type: LOAD_TWEETS_SUCCESS,
  tweets,
});

export const loadTweetsFailure = (err) => ({
  type: LOAD_TWEETS_FAILURE,
  errors: err,
});

export const loadTweets = (keywords) => (dispatch) => {
  dispatch(loadTweetsRequest(keywords));
  return api.tweets(keywords).then((tweets) => {
    dispatch(loadTweetsSuccess(tweets.data));
    return tweets.data;
  }).catch((err) => {
    dispatch(loadTweetsFailure(err));
    return Promise.reject(err);
  });
};