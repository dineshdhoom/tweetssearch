import {
  LOAD_TWEETS_REQUEST,
  LOAD_TWEETS_FAILURE,
  LOAD_TWEETS_SUCCESS,
} from '../../type';

const initialState = {
  tweets: []
};

export function tweetData(state = initialState, action) {
  switch (action.type) {
    case LOAD_TWEETS_REQUEST:
      return state;
    case LOAD_TWEETS_SUCCESS:
      return { tweets: action.tweets.statuses };
    case LOAD_TWEETS_FAILURE:
      return { ...state, errors: action.errors};
    default:
      return state;
  }
}
