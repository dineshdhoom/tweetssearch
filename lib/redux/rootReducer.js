import { combineReducers } from 'redux';
import { tweetData } from './reducers/tweet';

const rootReducer = combineReducers({
  tweetData
});

export default rootReducer;
