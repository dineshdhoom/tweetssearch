import React from 'react';
import ReactDOM from 'react-dom';
import routes from '../routes/clientRoute';

import { BrowserRouter as Router } from 'react-router-dom';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import reducers from '../redux/rootReducer';
import { composeWithDevTools } from 'redux-devtools-extension';

const store = createStore(reducers, window.__INITIAL_STATE__, composeWithDevTools(
  applyMiddleware(thunk),
));

ReactDOM.render(
  <Provider store={store}>
    <Router>
      {routes}
    </Router>
  </Provider>,
  document.getElementById('root')
);
