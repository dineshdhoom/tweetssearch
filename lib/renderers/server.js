import React from 'react';
import ReacDOMServer from 'react-dom/server';

import StaticRouter from 'react-router-dom/StaticRouter';
import { renderRoutes } from 'react-router-config';
import routes from '../routes/serverRoute';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import reducers from '../redux/rootReducer';

const store = createStore(reducers, applyMiddleware(thunk));


// const router = express.Router();

// import { App } from '../components';

const serverRenderer = () => {
  let context = {};
  return ReacDOMServer.renderToString(
    <Provider store={store}>
      <StaticRouter location='/' context={context}>
        {renderRoutes(routes)}
      </StaticRouter>
    </Provider>
  );
};

export default serverRenderer;