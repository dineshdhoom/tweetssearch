
import { Switch, Route } from 'react-router-dom';
import React from 'react';

import App from 'components/App';

import TweetPage from 'components/TweetPage';

export default (
  <Switch>
    <Route exact path='/' component={App}/>
    <Route exact path='/tweet/:id' component={TweetPage}/>
  </Switch>
);
