import App from 'components/App';

import TweetPage from 'components/TweetPage';


const routes = [
  { component: App,
    routes: [
      { path: '/',
        exact: true,
        component: App
      },
      { path: '/tweet/:id',
        component: TweetPage
      }
    ]
  }
];

export default routes;