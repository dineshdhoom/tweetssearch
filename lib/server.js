import express from 'express';
import config from 'config';
import serverRender from 'renderers/server';
import axios from 'axios';

const app = express();

app.use(express.static('public'));
app.set('view engine', 'ejs');

app.get('/', (req, res) => {
  const initialContent = serverRender();
  res.render('index', {initialContent});
});

app.get('/search/tweets', (req, res) => {
  const keywords = req.query.q;
  const authHeader = req.headers.authorization;
  axios(`${config.apiUrl}?q=${keywords}`, {
    method: 'GET',
    headers: {
      Authorization: authHeader,
    },
  }).then((response) => {
    res.send(response.data);
  }).catch((err) => {
    res.status(err.response.status).send('Something broke!');
  });
});

app.listen(config.port, function listenHandler(){
  console.info(`Running on ${config.port}...`);
});
