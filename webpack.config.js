const path = require('path');
const webpack = require('webpack');
const stylelint = require('stylelint');
const postcssNext = require('postcss-cssnext');
const postcssNested = require('postcss-nested');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const config = {
  entry: ['babel-polyfill', './lib/renderers/dom.js'],
  resolve: {
    modules: [path.resolve('./lib'), path.resolve('./node_modules')],
    extensions: ['.js', '.css', '.json']
  },
  output: {
    path: path.resolve(__dirname, 'public'),
    filename: 'bundle.js'
  },
  module: {
    rules: [
      { test: /\.js$/, exclude: /node_modules/, use: 'babel-loader' },
      {
        test: /\.css$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: 'css-loader?modules&importLoaders=2&localIdentName=[local]___[hash:base64:5]'
        })
      },
    ]
  },
  plugins: [
    new webpack.LoaderOptionsPlugin({
      debug: true,
      options: {
        tslint: {
          failOnHint: true
        },
        postcss: function () {
          return [
            stylelint({
              files: '../lib/components/*.css',
            }),
            postcssNext(),
            postcssNested(),
          ];
        },
      }
    }),
    new ExtractTextPlugin('app.css', { allChunks: true }),
  ]
};

module.exports = config;
